package com.gopas.java.assignment.domain.zoo;

public class Dog extends Animal {
    // fields
    // constructors
    // business logic methods
    // getters/setters, toString, equals/hashCode...
    public Dog(String title, String description) {
        super(title, description);
    }

    @Override
    public void sound() {
        // cesta k mp4. souboru, ulozenem v DB ci resources folderu
        System.out.println("Haf haf!");
    }
}
