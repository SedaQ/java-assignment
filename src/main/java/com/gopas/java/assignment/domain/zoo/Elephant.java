package com.gopas.java.assignment.domain.zoo;

public class Elephant extends Animal {

    public Elephant(String title, String description) {
        super(title, description);
    }

    @Override
    public void sound() {
        // cesta k mp4. souboru, ulozenem v DB ci resources folderu
        System.out.println("wohooo!");
    }
}
