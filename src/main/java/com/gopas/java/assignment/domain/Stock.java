package com.gopas.java.assignment.domain;

public class Stock {

    private Long id;
    private String city;
    private String description;
    private int capacity;
    private Product[] products = null;

    public Stock() {
    }

    public Stock(Long id, String city, String description, int capacity, Product[] products) {
        this.id = id;
        this.city = city;
        this.description = description;
        this.capacity = capacity;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public Product[] getProducts() {
        return products;
    }

    public void setProducts(Product[] products) {
        this.products = products;
    }
}
