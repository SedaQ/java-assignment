package com.gopas.java.assignment.domain.zoo;

public class Cat extends Animal implements Behavior {

    public Cat(String title, String description) {
        super(title, description);
    }

    @Override
    public void sound() {
        // cesta k mp4. souboru, ulozenem v DB ci resources folderu
        System.out.println("mnau!");
    }

    @Override
    public String eatingPreferences() {
        return "Musite ji to davat jen pokud je hladova..";
    }
}
