package com.gopas.java.assignment.domain.zoo;

public interface Behavior {

    String eatingPreferences();
}
