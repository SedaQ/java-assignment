package com.gopas.java.assignment.domain.zoo;

public abstract class Animal {
    private String title;
    private String description;

    public Animal(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public abstract void sound();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
