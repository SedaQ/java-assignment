package com.gopas.java.assignment.domain.zoo;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class App {

    public static void main(String[] args) {
        Animal cat = new Cat("cat", "kocka domaci");
        Animal dog = new Dog("dog", "pes divoky");
        Animal elephant = new Elephant("elephant", "slon ze zoo");

        List<Animal> animals = new ArrayList<>();
        animals.add(cat);
        animals.add(dog);
        animals.add(elephant);

        for (Animal animal : animals) {
            animal.sound();
        }

    }
}
