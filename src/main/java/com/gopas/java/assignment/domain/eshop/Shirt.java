package com.gopas.java.assignment.domain.eshop;

public class Shirt extends Clothing implements Printable {

    private String fit;

    public Shirt(String size, double price, String description, String fit) {
        super(size, price, description);
        this.fit = fit;
    }

    @Override
    public void display() {
//        super.display();
        System.out.println("Size: " + getSize());
        System.out.println("Price: " + getPrice());
        System.out.println("Description: " + getDescription());
        System.out.println("Fit: " + fit);
    }

    @Override
    public void print() {
        //.. print na tricko..
    }

    public String getFit() {
        return fit;
    }

    public void setFit(String fit) {
        this.fit = fit;
    }
}
