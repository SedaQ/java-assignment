package com.gopas.java.assignment.domain;

public class Product {
    private Long id;
    private String description;
    private String title;
    private double price;

    public Product() {
    }

    public Product(Long id, String description, String title, double price) {
        this.id = id;
        this.description = description;
        this.title = title;
        this.price = price;
    }

    public boolean isExpensive() {
        if (price > 500) {
            return true;
        }
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", price=" + price +
                ", expensive=" + isExpensive() +
                '}';
    }
}
