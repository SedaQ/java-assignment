package com.gopas.java.assignment.domain.eshop;

public interface Printable {

    void print();
}
