package com.gopas.java.assignment.domain.eshop;

public abstract class Clothing {

    private String size;
    private double price;
    private String description;

    public Clothing(String size, double price, String description) {
        this.size = size;
        this.price = price;
        this.description = description;
    }

    public void display() {
        System.out.println("Size: " + size);
        System.out.println("Price: " + price);
        System.out.println("Description: " + description);
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
