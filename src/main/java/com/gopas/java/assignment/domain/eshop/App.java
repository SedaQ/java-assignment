package com.gopas.java.assignment.domain.eshop;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {
        Clothing shirt = new Shirt("XL", 1000.0, "...", "OK");
        shirt.display();

        Printable printableProduct = new Shirt("XL", 1000.0, "...", "OK");

        List<Printable> printableProducts = new ArrayList<>();
        printableProducts.add(printableProduct);
    }
}
