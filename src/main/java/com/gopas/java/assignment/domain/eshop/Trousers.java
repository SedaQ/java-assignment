package com.gopas.java.assignment.domain.eshop;

public class Trousers extends Clothing {
    public Trousers(String size, double price, String description) {
        super(size, price, description);
    }
}
