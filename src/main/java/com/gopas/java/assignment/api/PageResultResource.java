package com.gopas.java.assignment.api;

import java.util.ArrayList;
import java.util.List;

public class PageResultResource<E> {
    private List<E> content = new ArrayList<>();
    private String pagination;


    public List<E> getContent() {
        return content;
    }

    public void setContent(List<E> content) {
        this.content = content;
    }

    public String getPagination() {
        return pagination;
    }

    public void setPagination(String pagination) {
        this.pagination = pagination;
    }
}
