package com.gopas.java.assignment.service;

public class AuditService {

    public <T extends Number> T audit(T t) {
        // ... some logic
        System.out.println("Audit t" + t);
        return t;
    }
}
