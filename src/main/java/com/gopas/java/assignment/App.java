package com.gopas.java.assignment;

import com.gopas.java.assignment.domain.Customer;
import com.gopas.java.assignment.domain.Product;
import com.gopas.java.assignment.domain.Stock;
import com.gopas.java.assignment.service.AuditService;

import java.time.*;
import java.util.ArrayList;

public class App {

    public static void main(String[] args) {
        System.out.println("Prvni argument z cmd: " + args[0]);

        int[][] myArray = new int[5][4];

        for (int i = 0; i < myArray.length; i++) {
            for (int j = 0; j < myArray[i].length; j++) {
                System.out.print(myArray[i][j] + " ");
            }
            System.out.println();
        }

//        LocalDate localDate = new LocalDate(1970, 1);
//        LocalDate localDate2 = new LocalDate(5, 3);

        LocalDate localDate3 = LocalDate.of(1970, 1, 1);

        // entry point aplikace

        // for cyklus -- kterej by mi z ResultSet (.. pohled na DB table)
        // ve vysledku pole produktu
        Product appleComputer1 = new Product();
        appleComputer1.setId(1L);
        appleComputer1.setDescription("Desc...");
        appleComputer1.setPrice(5);
        appleComputer1.setTitle("Title...");

        Product appleComputer2 = new Product();
        appleComputer2.setId(2L);
        appleComputer2.setDescription("Descasdasd...");
        appleComputer2.setPrice(50);
        appleComputer2.setTitle("Title.asdasd..");

        // customer -- tyhle produkty..
        Customer customer1 = new Customer(1L, "karel");

        ArrayList<Product> productsList = new ArrayList<>();
        productsList.add(appleComputer1);
        productsList.add(appleComputer2);

        productsList.remove(appleComputer2);
        productsList.size();

        customer1.setProducts(productsList);

        Product[] stockProducts = {appleComputer1, appleComputer2};

        String firstName = "aa";
        String lastName = "ll";
        String nickname = "aaa";
        String email = "email...";

        ArrayList<String> userMetadata = new ArrayList<>();
        userMetadata.add(firstName);
        userMetadata.add(lastName);
        userMetadata.add(nickname);
        userMetadata.add(email);
        userMetadata.remove("aa");
        for (int i = 0; i < userMetadata.size(); i++) {
            System.out.println(userMetadata.get(i));
        }


        Stock stock = new Stock();
        stock.setCapacity(5);
        stock.setCity("Praha");
        stock.setDescription("Maly sklad v Praze");
        stock.setId(1L);
        stock.setProducts(stockProducts);

        Product[] productsInStock = stock.getProducts();
        for (Product product : productsInStock) {
            System.out.println(product);
        }

        AuditService auditService = new AuditService();
        auditService.audit(5);

        LocalDateTime localDateTime = LocalDateTime.now(Clock.systemUTC());
        LocalDate localDate = LocalDate.now();
        localDate.plusDays(5);

        LocalTime localTime;
        Instant instant;
        Duration duration;
        Period period;

        System.out.println(localDateTime);

        String month = "LEDEN";

        switch (month) {
            case "LEDEN": {
                System.out.println("Je leden...");
                break;
            }
            case "UNOR": {
                System.out.println("Je unor...");
                break;
            }
            default: {
                System.out.println("Ani jeden mesic nebyl matchnut..");
            }
        }
    }
}
